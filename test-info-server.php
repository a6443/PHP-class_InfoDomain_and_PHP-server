<?php
/* DomainInfo ==╗…….start ☢
+ ----© Ardilla-CMS 2023 -------------------------------+
|   Domain Name, Domain ID, WHOIS Server, Registrar URL, Updated Date, Creation Date, Registry Expiry Date, Domain, DNS Records, IP Address, MX, NS.
|     ❶ ⟨uk⟩ - Виводить інформацію про ваш домен
|     ❷ ⟨pl⟩ — Wyświetla informacje o Twojej domenie
|     ❸ ⟨en⟩ - Displays information about your domain.
+-------------------------------------------------------+
*/
class DomainInfo {
    private $domain;
    public function __construct($domain) {$this->domain = $domain;}
    public function get_info() {
        //--------------------
        $domain = $this->domain;
        $whois_server = 'whois.nic.host';
        $port = 43;
        $timeout = 10;

        $fp = fsockopen($whois_server, $port, $errno, $errstr, $timeout);
        if (!$fp) {
            echo "Error: $errno - $errstr";
        } else {
            $query = $domain . "\r\n";
            fputs($fp, $query);
            $response = '';
            while (!feof($fp)) {
                $response .= fgets($fp, 128);
            }
            fclose($fp);

            if (strpos($response, 'Creation Date:') !== false) {
                //$creation_date = trim(str_replace('Creation Date:', '', strstr($response, 'Creation Date:')));
                //$substring = strstr($creation_date, "UAB Registrar IANA ID", true);

                // отримуємо строку - вирізаємо лишне і додаєм перенос слів нанову строку
                $status_pos = strpos($response, "Status:");
                $textinfwhois = substr($response, 0, $status_pos);
                $keywords = array("Domain Name:", "Registry Domain ID:", "Registrar WHOIS Server:", "WHOIS Server:", "Registrar URL:", "Updated Date:", "Creation Date:", "Registry Expiry Date:", "Registrar:", "UAB Registrar IANA ID");
                foreach ($keywords as $keyword) {
                    $textinfwhois = str_replace($keyword, '<br>'.$keyword, $textinfwhois);
                }
                // Додати теги <b> та </b> перед і після ':' у кожному рядку
                $textinfwhois = str_replace("<br>", "<br><b>", $textinfwhois);
                $textinfwhois = str_replace(":", ":</b>", $textinfwhois);

                echo $textinfwhois;

            } else {
                echo 'Інформація про дату реєстрації домену ' . $domain . ' не знайдена.';
            }
        }
        //------------------------------
        $ip = gethostbyname($this->domain);
        $host = gethostbyaddr($ip);
        $dns_records = dns_get_record($this->domain);
        $whois = $this->get_whois_info($this->domain);
        $info = '<ul class="domen-glav">';
        $info .= '<li><strong>Domain:</strong> ' . $this->domain . '</li>';
        $info .= '<li><strong>IP Address:</strong> ' . $ip . '</li>';
        $info .= '<li><strong>Host:</strong> ' . $host . '</li>';
        $info .= '<li><strong>DNS Records:</strong> <ul>';
        foreach ($dns_records as $record) {foreach ($record as $i) {$info .= '<span>' . print_r($i, true) . '</span>';}}
        $info .= '</ul></li>';
        $info .= '</ul>';
        return $info;
    }
    public function getIP() {$ip = gethostbyname($this->domain); return '<li>IP Address: ' . $ip . '</li>';}
    public function getMXRecords() {
        $dns_records = dns_get_record($this->domain, DNS_MX);    $output = '';
        foreach ($dns_records as $record) {$output .= '<li>' . $record['type'] . ': ' . $record['target'] . '</li>';}
        return $output;
    }
    public function getNameservers() {
        $dns_records = dns_get_record($this->domain, DNS_NS);    $output = '';
        foreach ($dns_records as $record) {$output .= '<li>' . $record['type'] . ': ' . $record['target'] . '</li>';}
        return $output;
    }
    private function get_whois_info($domain) {
        $whois_server = 'whois.iana.org';   $whois_info = '';
        $conn = fsockopen($whois_server, 43, $errno, $errstr, 10);
        if ($conn) {
            fputs($conn, $domain . "\r\n");
            while (!feof($conn)) {$whois_info .= fgets($conn, 128);}
            fclose($conn);
        }
        return $whois_info;
    }
}
/* DomainInfo ==╗…….end ☕
+ --------------║------- -------------------------------+
|     ❶ ⟨uk⟩ - Вивід методів класу (➥ Замініть домен "ardilla.host" на свій)
|     ❷ ⟨pl⟩ — Wyprowadzenie metod klasowych (➥ Zastąp domenę „ardilla.host” własną)
|     ❸ ⟨en⟩ - Derivation of class methods  (➥ Replace the domain "ardilla.host" with your own)
+-------------------------------------------------------+
*/
$domain_info = new DomainInfo('ardilla.host');
echo '<ul>';
echo $domain_info->get_info();
echo $domain_info->getIP();
echo $domain_info->getMXRecords();
echo $domain_info->getNameservers();
echo '</ul>';


/* ServerInfo ==╗…….start ☢
+ ----© Ardilla-CMS 2023 -------------------------------+
|     ❶ ⟨uk⟩ - Клас виводить інформацію про PHP сервер: Метод "displayInfo()" складається із 4 інформаційних блоків: $apach_modul, $php_modul, $test_modul, $css_modul.
|     ❷ ⟨pl⟩ — Klasa wyświetla informacje o serwerze PHP: Metoda „displayInfo()” składa się z 4 bloków informacyjnych: $apach_modul, $php_modul, $test_modul, $css_modul.
|     ❸ ⟨en⟩ - The class displays information about the PHP server: The "displayInfo()" method consists of 4 information blocks: $apach_modul, $php_modul, $test_modul, $css_modul.
+-------------------------------------------------------+
*/
class ServerInfo
{
    private $language;
    //๑۩۩๑==⮟=Перевірка чи це локальна машина
    public static function is_local_ip($ip) {
        return preg_match('/^127\./', $ip) || preg_match('/^::1/', $ip) ||
            preg_match('/^192\.168\./', $ip) || preg_match('/^10\./', $ip) ||
            preg_match('/^172\.(1[6-9]|2[0-9]|3[0-1])\./', $ip);
    }

    //๑۩۩๑==⮟=Массив з перекладом
    private $translations = [
        'en' => [
            'title' => 'Server information', 'veraphp' => 'PHP version',  'timzone' => 'Time zone', 'mons' => 'Month',
            'year' => 'Year', 'time' => 'Time',  'mempool_default' => 'Default memory size for MySQLND memory pool',
            'max_accelerated_f' => 'Maximum number of files that can be accelerated by OPCache',
            'memory_consumption' => 'Maximum amount of memory that OPCache can use',
            'output_buffering' => 'Output buffer size', 'memory_limit' => 'The maximum amount of RAM that PHP can use',
            'max_input_time' => 'Maximum query execution time', 'input_nesting' => 'Maximum nesting level of input data',
            'max_file_uploads' => 'Maximum number of files for uploading',
            'post_max_size' => 'Maximum size of data that can be sent to the server using the POST method in PHP',
            'memgig' => 'Amount of memory used:', 'diskgg' => 'Amount of free disk space:',
            'ip' => 'IP address', 'hostname' => 'Host name', 'software' => 'Software',
            'protocol' => 'Protocol', 'sslyes' => 'SSL certificate connected',
            'sslno' => 'SSL certificate is missing!', 'domenhttp'=> 'Your domain',
            'allmodapache' => 'List of all enabled modules','allmodphp' => 'List of all enabled PHP extensions',
        ],
        'uk' => [
            'title' => 'Інформація про сервер', 'veraphp' => 'Версія PHP', 'timzone' => 'Часова зона',
            'mons' => 'Місяць', 'year' => 'Рік', 'time' => 'Час', 'mempool_default' => 'Розмір пам\'яті за замовчуванням для мемпула MySQLND',
            'max_accelerated_f' => 'Максимальна кількість файлів, які можуть бути прискорені OPCache',
            'memory_consumption' => 'Максимальний обсяг пам\'яті, який може використовувати OPCache',
            'output_buffering' => 'Розмір буфера виведення', 'memory_limit' => 'Максимальний обсяг оперативної пам\'яті, який може використовувати PHP',
            'max_input_time' => 'Максимальний час виконання запиту', 'input_nesting' => 'Максимальний рівень вкладеності введених даних',
            'max_file_uploads' => 'Максимальна кількість файлів для завантаження',
            'post_max_size' => 'Максимальний розмір даних, який можна надіслати на сервер методом POST в PHP',
            'memgig' => 'Кількість використаної пам\'яті:','diskgg' => 'Кількість вільного місця на диску:',
            'ip' => 'IP адреса', 'hostname' => 'Назва хоста', 'software' => 'Програмне забезпечення',
            'protocol' => 'Протокол', 'sslyes' => 'SSL-сертифікат підключено',
            'sslno' => 'SSL-сертифікат відсутній!', 'domenhttp' => 'Ваш домен','allmodapache' => 'Список усіх увімкнутих модулів',
            'allmodphp' => 'Список усіх увімкнутих розширень РНР',
            'test_time_sdelat' => 'Час виконання тесту',
            'test_memori_limit' => 'Використано оперативки',
            'test_title' => 'Тест на швидкодію Вашого сервера',
            'test_deskription' => 'Тест створює масив на  300 000 ідентифікаторів з значенями від 10 до 20 знаків. Відсортовується за кількістю знаків від меньшого до більшого',
            'test_h3' => 'Пояснення до тесту',
            'poys_n1' => '1. Якщо час виконання тесту більше 1 секунди - Ваш сервер нижче середнього 👎',
            'poys_n2' => '2. Якщо під час тесту Ваш сервер використав оперативки більше 33мб - це свідчить про неправильне налаштування серверу 👎',
        ],
        'pl' => [
            'title' => 'Informacje o serwerze', 'veraphp' => 'Wersja PHP', 'timzone' => 'Strefa czasowa',
            'mons' => 'Miesiąc', 'year' => 'Rok',  'time' => 'Czas', 'mempool_default' => 'Domyślny rozmiar pamięci dla mempoola MySQLND',
            'max_accelerated_f' => 'Maksymalna liczba plików, które mogą być przyspieszone przez OPCache',
            'memory_consumption' => 'Maksymalny rozmiar pamięci, jaki może być wykorzystany przez OPCache',
            'output_buffering' => 'Rozmiar bufora wyjściowego',  'memory_limit' => 'Maksymalna ilość pamięci operacyjnej, jaką może wykorzystać PHP',
            'max_input_time' => 'Maksymalny czas wykonania żądania', 'input_nesting' => 'Maksymalny poziom zagnieżdżenia danych wejściowych',
            'max_file_uploads' => 'Maksymalna liczba plików do przesłania',
            'post_max_size' => 'Maksymalna wielkość danych, które można przesłać na serwer za pomocą metody POST w PHP',
            'memgig' => 'Ilość używanej pamięci:', 'diskgg' => 'Ilość wolnego miejsca na dysku:', 'ip' => 'Adres IP', 'hostname' => 'Nazwa hosta', 'software' => 'Oprogramowanie',
            'protocol' => 'Protokół', 'sslyes' => 'Certyfikat SSL podłączony', 'sslno' => 'Certyfikat SSL nieobecny!',
            'domenhttp'=> 'Twoja domena', 'allmodapache' => 'Lista wszystkich włączonych modułów',
            'allmodphp' => 'Lista wszystkich włączonych rozszerzeń PHP',
        ],
    ];

    public function __construct($language){$this->language = $language;}
    //๑۩۩๑==⮟=Повертає окремі пункти
    public function getItem($key){return $this->translations[$this->language][$key] ?? null;}
    //๑۩۩๑==⮟=Візуальний вивід
    public function displayInfo($apach_modul = null, $php_modul = null, $test_modul = null, $css_modul = null)
    {
        $ip = $_SERVER['REMOTE_ADDR'];

        echo '<h2>📘 ' . $this->getItem('title') . '</h2>';
        echo '<ul class="generally-about">';
        $phpVersion = PHP_VERSION;
        if (version_compare($phpVersion, '8.0', '>=')) {echo "<p><strong>🐘 " . $this->getItem('veraphp') . ": </strong>" . PHP_VERSION;}
        else {echo "<p><strong>🐘 " . $this->getItem('veraphp') . ": </strong>" . PHP_VERSION;}
        $list = "<li><strong>" . $this->getItem('timzone') . ":</strong> " . date("T") . '</li>';
        $list .= "<li><strong>" . $this->getItem('mons') . ":</strong> " . date('F') . '</li>';
        $list .= "<li><strong>" . $this->getItem('year') . ":</strong> " . date('Y') . '</li>';
        $list .= "<li><strong>" . $this->getItem('time') . ":</strong> " . date('H:i:s') . '</li>';
        $list .= "<li><strong>" . $this->getItem('memgig') . ":</strong> " . round(memory_get_usage() / 1024 / 1024, 2) . ' MB</li>';
        if (self::is_local_ip($ip)){}else{
            $list .= "<li><strong>" . $this->getItem('diskgg') . ":</strong> " . round(disk_free_space('/') / 1024 / 1024 / 1024, 2) . ' GB</li>';
        }
        $list .= "<li><strong>" . $this->getItem('post_max_size') . ":</strong> " . ini_get('post_max_size') . '</li>';
        $list .= "<li><strong>" . $this->getItem('mempool_default') . ":</strong> " . ini_get('mysqlnd.mempool_default_size') . '</li>';
        $list .= "<li><strong>" . $this->getItem('max_accelerated_f') . ":</strong> " . ini_get('opcache.max_accelerated_files') . '</li>';
        $list .= "<li><strong>" . $this->getItem('memory_consumption') . ":</strong> " . ini_get('opcache.memory_consumption') . '</li>';
        $list .= "<li><strong>" . $this->getItem('output_buffering') . ":</strong> " . ini_get('output_buffering') . '</li>';
        $list .= "<li><strong>" . $this->getItem('memory_limit') . ":</strong> " . ini_get('memory_limit') . '</li>';
        $list .= "<li><strong>" . $this->getItem('max_input_time') . ":</strong> " . ini_get('max_input_time') . '</li>';
        $list .= "<li><strong>" . $this->getItem('input_nesting') . ":</strong> " . ini_get('max_input_nesting_level') . '</li>';
        $list .= "<li><strong>" . $this->getItem('max_file_uploads') . ":</strong> " . ini_get('max_file_uploads') . '</li>';
        $list .= "<li><strong>" . $this->getItem('ip') . ":</strong> " . $_SERVER['SERVER_ADDR'] . "</li>";
        $list .= "<li><strong>" . $this->getItem('hostname') . ":</strong> " . $_SERVER['SERVER_NAME'] . "</li>";
        $list .= "<li><strong>" . $this->getItem('software') . ":</strong> " . $_SERVER['SERVER_SOFTWARE'] . "</li>";
        $list .= "<li><strong>" . $this->getItem('protocol') . ":</strong> " . $_SERVER['SERVER_PROTOCOL'] . "</li>";
        echo $list;

        if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS']){
            echo '<li><strong style="color:green;">' . $this->getItem('sslyes') . ' : ✔️</strong></li>';
        } else{
            echo '<li><strong style="color:red; font-weight: bolder;">' .  $this->getItem('sslno') . ' : 🚨</strong></li>';
        }
        if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS']){
            echo '<li><strong>' .  $this->getItem('domenhttp') . ' : 🏢 </strong> https://' .  $_SERVER['HTTP_HOST'] . '</li>';
        } else{
            echo '<li><strong>' .  $this->getItem('domenhttp') . ' : 🏠 </strong> http://' .  $_SERVER['HTTP_HOST'] . '</li>';
        }
        echo '</ul>';

        if (isset($apach_modul) && $apach_modul == 1) {
            //๑۩۩๑==⮟==Список усіх увімкнутих ОСНОВНИХ модулів apach
            echo '<h2>📒 ' . $this->getItem('allmodapache') .' '. $_SERVER['SERVER_SOFTWARE'] . ':</h2>';
            $modules = apache_get_modules();
            echo '<ul class="apach-about">';
            foreach($modules as $module) {echo "<li>$module</li>";}
            echo '</ul>';
        }

        if (isset($php_modul) && $php_modul == 1) {
            //๑۩۩๑==⮟==Список усіх увімкнутих розширень РНР
            echo '<h2>📙 ' . $this->getItem('allmodphp') . ':</h2>';
            $extensions = get_loaded_extensions();
            echo '<ul class="php-about">';
            foreach($extensions as $extension) {echo "<li>$extension</li>";}
            echo '</ul>';
        }

        if (isset($test_modul) && $test_modul == 1) {
            //๑۩۩๑==⮟=TEST

            function file_size($size){$filesizename = array(' Bytes', ' KB', ' MB', ' GB', ' TB', ' PB', ' EB', ' ZB', ' YB'); return $size ? round($size/pow(1024, ($i = floor(log($size, 1024)))), 2) .$filesizename[$i] : '0 Bytes'; }
            $mem_start = memory_get_usage(); $time_start = microtime(true);
            $array = [];
            for ($i = 0; $i < 300000; $i++) {
                $length = rand(10, 20);
                $value = substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
                $array[] = $value;
            }
            usort($array, function($a, $b) {return strlen($a) - strlen($b);});
            echo '<h2>📕 ' . $this->getItem('test_title') . ':</h2>';
            echo '<div class="test-block">';
            echo '<i>' .  $this->getItem('test_deskription') . '</i></br>';

            if("round(microtime(true) - $time_start, 4)" > 1){
                echo '<span>' .  $this->getItem('test_time_sdelat') . ': <b>' . round(microtime(true) - $time_start, 4) . '</b> sec. <sup>(Тест пройдено✔ 👏)<sup></span></br>';
            }else{
                echo '<span>' .  $this->getItem('test_time_sdelat') . ': <b>' . round(microtime(true) - $time_start, 4) . '</b> sec. <sup>(Тест не пройдено🚨)<sup></span></br>';
            }

            if("file_size(memory_get_usage() - $mem_start)" > 33){
                echo '<span>' .  $this->getItem('test_memori_limit') . ': <b>' . file_size(memory_get_usage() - $mem_start) . '</b> sec. <sup>(Тест пройдено✔ 👏)<sup></span></br>';
            }else{
                echo '<span>' .  $this->getItem('test_memori_limit') . ': <b>' . file_size(memory_get_usage() - $mem_start) . '</b>  <sup>(Тест не пройдено🚨)<sup></span></br>';
            }


            echo '<div class="test-poyasn">';
            echo '<h3>' .  $this->getItem('test_h3') . '</h3>';
            echo '<p>'. $this->getItem('poys_n1') .'</p>';
            echo '<p>'. $this->getItem('poys_n2') .'</p>';
            echo '</div></div>';
            unset($size, $array, $value, $length, $mem_start, $filesizename);

        }

        if (isset($css_modul) && $css_modul == 1) {
            //๑۩۩๑==⮟=Виводить стилі для оформлення при потребі
            function displayCSS(){
                echo <<<HTML
                        <style>
                            .php-about {font-family: 'Trebuchet MS', Helvetica, sans-serif; padding: 0; margin:20px; display:flex; flex-flow: row wrap; justify-content: space-between; gap: 1rem;}
                            .php-about li {list-style: none; margin:0; border-radius: 6px; padding:2px 4px; background: #95bba7;  }
                            .apach-about {margin:20px; padding: 0; display:flex; flex-flow: row wrap; justify-content: space-between; gap: 1rem; }
                            .apach-about li {list-style: none; margin:0; border-radius: 6px; padding:2px 4px; background: peachpuff;}
                            .generally-about { line-height:1; font-family: 'Trebuchet MS', Helvetica, sans-serif; font-size:16px; padding-left: 20px; font-weight: bolder;}
                            .generally-about strong{font-weight: lighter; color: #3D3D3D;font-size:13px;}
                            .domen-about {}
                            .test-block {background: #3D3D3D; padding: 20px; border-radius: 10px; color: whitesmoke;}
                            .test-block i, .test-poyasn {display:block; margin-bottom:10px; background: #9a9a9b; padding: 10px; border-radius: 10px; color: #3a2434;}
                            .test-block sup, .test-block sub{background: #00bb00; color:yellow ; font-weight: bold; padding:2px 4px; border-radius: 4px; }
                            .test-block sub{background: darkred;}
                            .test-block b {font-size: 1.4em;}
                            .test-poyasn{margin-top:20px;}
                        </style>
HTML;
            }
            displayCSS();
        }
    }
}
/* ServerInfo ==╗…….end ☕
+ --------------║------- -------------------------------+
|     ❶ ⟨uk⟩ - 0 ➥ Не виводи блок |  1 ➥ Показати блок  |  uk або pl або en ➥ Мова |
|     ❷ ⟨pl⟩ — 0 ➥ Nie wyprowadzaj bloku | 1 ➥ Pokaż blok | uk lub pl lub en ➥ Język |
|     ❸ ⟨en⟩ - 0 ➥ Do not output block | 1 ➥ Show block | uk or pl or en ➥ Language |
+-------------------------------------------------------+
*/
//Виконання класу
$apach_modul = 1;
$php_modul = 1;
$test_modul = 1; // Запускає тест сервера   |  Uruchamia test serwera  |  Runs the server test
$css_modul = 1; // Візуалізує блоки  |   Wizualizuje bloki   | Visualizes blocks  |
$serverInfo = new ServerInfo('uk');
$serverInfo->displayInfo($apach_modul, $php_modul, $test_modul, $css_modul);


//⟨uk⟩ ($test_modul) - Тест створює масив на  300 000 ідентифікаторів з значенями від 10 до 20 знаків. Відсортовується за кількістю знаків від меньшого до більшого.
//⟨pl⟩ ($test_modul) - Test tworzy tablicę 300 000 identyfikatorów o wartościach od 10 do 20 znaków. Posortowane według liczby znaków od najmniejszej do największej.
//⟨en⟩ ($test_modul) - The test creates an array of 300,000 identifiers with values from 10 to 20 characters. Sorted by the number of characters from least to most.
?>

